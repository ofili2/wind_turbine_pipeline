import unittest
import logging
from unittest.mock import MagicMock
from pymongo import MongoClient

from collection_tier.event_logs.buffered_handler import LevelFilter, MongoDBBufferedHandler


class TestMongoDBBufferedHandler(unittest.TestCase):
    def setUp(self) -> None:
        self.client = MongoClient()
        self.database = self.client.test_db
        self.collection = self.database.test_collection
        self.handler = MongoDBBufferedHandler('localhost', 27017, 'test_db', 'test_collection', buffer_size=2)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        self.logger.addHandler(self.handler)
    
    def tearDown(self) -> None:
        self.client.drop_database('test_db')
        self.client.close()

def test_log_record_written_to_buffer(self):
    # Given a log record
    record = logging.LogRecord(
        name='test_logger',
        level=logging.DEBUG,
        pathname='test_pathname',
        lineno=1,
        msg='test_message',
        args=None,
        exc_info=None,
        func=None
    )

    # When the log record is emitted
    self.logger.handle(record)

    # Then the log record should be written to the buffer
    self.assertEqual(len(self.handler.buffer), 1)

def test_buffer_flushed_to_collection(self):
    # Given log records in the buffer
    self.handler.buffer = [
        {
            'timestamp': 1648922347.2882972,
            'level': 'INFO',
            'message': 'test message 1'
        },
        {
            'timestamp': 1648922348.2882972,
            'level': 'ERROR',
            'message': 'test message 2'
        }
    ]

    # When the buffer is flushed
    self.handler.flush()

    # Then the log records should be written to the collection
    records = list(self.collection.find())
    self.assertEqual(len(records), 2)
    self.assertEqual(records[0]['message'], 'test message 1')
    self.assertEqual(records[1]['message'], 'test message 2')


class TestLevelFilter(unittest.TestCase):
    def test_filter(self):
        # Given a log record and a filter
        record = MagicMock()
        record.levelno = logging.INFO
        filter_ = LevelFilter(logging.INFO)
        # When the log record is filtered
        result = filter_.filter(record)

        # Then the filter should return True
        self.assertTrue(result)

    def test_filter_returns_false(self):
        # Given a log record and a filter
        record = MagicMock()
        record.levelno = logging.INFO
        filter_ = LevelFilter(logging.ERROR)

        # When the log record is filtered
        result = filter_.filter(record)

        # Then the filter should return False
        self.assertFalse(result)
