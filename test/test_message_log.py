import os
import shutil
import tempfile
import unittest

from collection_tier.message_log import MessageLogger


class TestMessageLogger(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        MessageLogger.transient_state_db_path = os.path.join(self.test_dir, 'test_transient.db')
        MessageLogger.failed_state_db_path = os.path.join(self.test_dir, 'test_failed.db')
        MessageLogger.ensure_directories()
        MessageLogger.initialize()

    def tearDown(self):
        shutil.rmtree(self.test_dir)
        MessageLogger.close()

    def test_log_transient_state(self):
        key = 'test_key'
        value = 'test_value'
        MessageLogger.log_transient_state(key, value)
        self.assertEqual(MessageLogger.get_transient_state(key), value.encode())

    def test_log_failed_state(self):
        key = 'test_key'
        value = 'test_value'
        MessageLogger.log_failed_state(key, value)
        self.assertEqual(MessageLogger.get_failed_state(key), value.encode())

    def test_add_event(self):
        key = 'test_key'
        data = 'test_data'
        MessageLogger.log_transient_state(key, data)
        MessageLogger.add_event(key, data)
        self.assertEqual(MessageLogger.get_transient_state(key), data.encode())

    def test_remove_event(self):
        key = 'test_key'
        data = 'test_data'
        MessageLogger.log_transient_state(key, data)
        MessageLogger.remove_event(key)
        self.assertIsNone(MessageLogger.get_transient_state(key))

    def test_moved_to_failed(self):
        key = 'test_key'
        data = 'test_data'
        MessageLogger.log_transient_state(key, data)
        MessageLogger.moved_to_failed(key)
        self.assertEqual(MessageLogger.get_failed_state(key), data.encode())
        self.assertIsNone(MessageLogger.get_transient_state(key))
