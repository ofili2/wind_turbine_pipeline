import unittest
from datetime import datetime
from cassandra.cluster import NoHostAvailable
from collection_tier.cassandra_writer import CassandraWriter


class TestCassandraWriter(unittest.TestCase):
    def setUp(self):
        # Initialize a test instance of CassandraWriter
        self.writer = CassandraWriter(
            host="localhost",
            port=9042,
            keyspace="test_keyspace",
            table="test_table",
            username="test_user",
            password="test_password",
        )

    def test_write_success(self):
        # Test writing a valid message to Cassandra
        message = {
            "timestamp": datetime.utcnow().timestamp(),
            "generator___mechanical___speed": 12.3,
            "generator___temperature": 45.6,
            "turbine_data___electrical___power_production": 78.9,
            "met_sensors___mechanical___wind_speed": 10.2,
            "met_sensors___temperature___ambient": 30.4,
        }
        self.writer.write(message)

    def test_write_failure(self):
        # Test handling a NoHostAvailable error when writing to Cassandra
        message = {
            "timestamp": datetime.utcnow().timestamp(),
            "generator___mechanical___speed": 12.3,
            "generator___temperature": 45.6,
            "turbine_data___electrical___power_production": 78.9,
            "met_sensors___mechanical___wind_speed": 10.2,
            "met_sensors___temperature___ambient": 30.4,
        }
        # Force a NoHostAvailable error by providing a non-existent host
        self.writer.connector.host = "non_existent_host"
        with self.assertRaises(NoHostAvailable):
            self.writer.write(message)

    def tearDown(self):
        # Clean up after each test by closing the Cassandra session
        self.writer.connector.close()


if __name__ == "__main__":
    unittest.main()
