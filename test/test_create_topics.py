"""
These tests check that create_topics creates the expected NewTopic objects and calls the create_topics method of KafkaAdminClient, 
and that it logs the appropriate messages. It also checks that the function behaves correctly when a TopicAlreadyExistsError or another exception is raised.

Note that in order to test the function properly, we need to use unittest.mock.patch to mock the path, 
KafkaAdminClient, and logging modules, since those are external dependencies that we don't want to rely on during testing.
"""

import unittest
from unittest.mock import patch, MagicMock

from kafka.admin import KafkaAdminClient, NewTopic
from kafka.errors import TopicAlreadyExistsError

from collection_tier.create_topics import create_topics


class TestCreateTopics(unittest.TestCase):
    @patch('my_module.path')
    @patch('my_module.KafkaAdminClient')
    @patch('my_module.logging')
    def test_create_topics_success(self, mock_logging, mock_admin_client, mock_path):
        # Arrange
        mock_path.abspath.return_value = '/my/config/path'
        mock_path.join.return_value = '/my/config/path/logging.ini'
        mock_admin_client.return_value = MagicMock()
        mock_logger = MagicMock()
        mock_logging.getLogger.return_value = mock_logger

        # Act
        create_topics()

        # Assert
        mock_admin_client.assert_called_once_with(
            bootstrap_servers=['my-bootstrap-server'],
        )
        mock_admin_client.return_value.create_topics.assert_called_once_with([
            NewTopic(name='my-topic', num_partitions=3, replication_factor=2)
        ])
        mock_logger.info.assert_called_once_with('1 topics created successfully: my-topic')

    @patch('my_module.path')
    @patch('my_module.KafkaAdminClient')
    @patch('my_module.logging')
    def test_create_topics_topic_already_exists(self, mock_logging, mock_admin_client, mock_path):
        # Arrange
        mock_path.abspath.return_value = '/my/config/path'
        mock_path.join.return_value = '/my/config/path/logging.ini'
        mock_admin_client.return_value = MagicMock()
        mock_admin_client.return_value.create_topics.side_effect = TopicAlreadyExistsError(
            "Topic 'my-topic' already exists."
        )
        mock_logger = MagicMock()
        mock_logging.getLogger.return_value = mock_logger

        # Act
        create_topics()

        # Assert
        mock_admin_client.assert_called_once_with(
            bootstrap_servers=['my-bootstrap-server'],
        )
        mock_admin_client.return_value.create_topics.assert_called_once_with([
            NewTopic(name='my-topic', num_partitions=3, replication_factor=2)
        ])
        mock_logger.warning.assert_called_once_with("Topic(s) ['my-topic'] already exist(s). Skipping creation.")

    @patch('my_module.path')
    @patch('my_module.KafkaAdminClient')
    @patch('my_module.logging')
    def test_create_topics_error(self, mock_logging, mock_admin_client, mock_path):
        # Arrange
        mock_path.abspath.return_value = '/my/config/path'
        mock_path.join.return_value = '/my/config/path/logging.ini'
        mock_admin_client.return_value = MagicMock()
        mock_admin_client.return_value.create_topics.side_effect = Exception("Something went wrong.")
        mock_logger = MagicMock()
        mock_logging.getLogger.return_value = mock_logger

        # Act & Assert
        with self.assertRaises(Exception):
            create_topics()
        mock_admin_client.assert_called_once_with(
            bootstrap_servers=['my-bootstrap-server'],
        )
        mock_admin_client.return_value.create_topics.assert_called_once_with([
            NewTopic(name='my-topic', num_partitions=3, replication_factor=2)
        ])
        mock_logger.error.assert_called_once_with('An error occurred while creating topics: Something went wrong.')
