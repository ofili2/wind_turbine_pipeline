import unittest
from unittest.mock import MagicMock, patch

from pyspark.sql import Row

from analysis_tier.influxdb_writer import InfluxDBWriter


class TestInfluxDBWriter(unittest.TestCase):

    @patch('influxdb_writer.InfluxDBClient')
    def test_process(self, mock_client):
        writer = InfluxDBWriter()
        mock_write_api = MagicMock()
        mock_client.return_value.write_api.return_value = mock_write_api

        row = Row(
            start='2022-03-31T16:00:00Z',
            end='2022-03-31T16:10:00Z',
            generator___mechanical___speed=14.5,
            generator___temperature=62.3,
            turbine_data___electrical___power_production=5000,
            met_sensors___mechanical___wind_speed=8.3,
            met_sensors___temperature___ambient=23.5,
        )

        writer.process(row)

        mock_write_api.write.assert_called_once_with(
            bucket='energydata',
            record=[{
                "measurement": "energydata",
                "time": '2022-03-31T16:00:00Z',
                "fields": {
                    "generator_mechanical_speed": 14.5,
                    "generator_temperature": 62.3,
                    "turbine_power_production": 5000,
                    "wind_speed": 8.3,
                    "ambient_temperature": 23.5,
                },
                "tags": {
                    "end_time": '2022-03-31T16:10:00Z',
                },
            }],
            write_precision='ns',
        )

    def test_validate_row_missing_fields(self):
        writer = InfluxDBWriter()
        row = Row(start='2022-03-31T16:00:00Z')

        with self.assertRaises(ValueError):
            writer.validate_row(row)

    def test_validate_row_invalid_time_format(self):
        writer = InfluxDBWriter()
        row = Row(
            start='2022-03-31T16:00:00',
            end='2022-03-31T16:10:00Z',
            generator___mechanical___speed=14.5,
            generator___temperature=62.3,
            turbine_data___electrical___power_production=5000,
            met_sensors___mechanical___wind_speed=8.3,
            met_sensors___temperature___ambient=23.5,
        )

        with self.assertRaises(ValueError):
            writer.validate_row(row)

    def test_validate_row_invalid_measurement_name(self):
        writer = InfluxDBWriter()
        row = Row(
            start='2022-03-31T16:00:00Z',
            end='2022-03-31T16:10:00Z',
            measurement='invalid-measurement-name',
            generator___mechanical___speed=14.5,
            generator___temperature=62.3,
            turbine_data___electrical___power_production=5000,
            met_sensors___mechanical___wind_speed=8.3,
            met_sensors___temperature___ambient=23.5,
        )

        with self.assertRaises(ValueError):
            writer.validate_row(row)

    def test_validate_row_fields_not_dict(self):
        writer = InfluxDBWriter()
        row = Row(
            start='2022-03-31T16:00:00Z',
            end='2022-03-31T16:10:00Z',
            measurement='energydata',
            fields='invalid-fields-type',
            tags={'tag1': 'value1'},
        )

        with self.assertRaises(ValueError):
            writer.validate_row(row)

def test_validate_row(self):
    writer = InfluxDBWriter()
    row = Row(
        start='2022-03-31T16:00:00Z',
        end='2022-03-31T16:10:00Z',
        generator___mechanical___speed=14.5,
        generator___temperature=62.3,
        turbine_data___electrical___power_production=5000,
        met_sensors___mechanical___wind_speed=8.3,
        met_sensors___temperature___ambient=23.5,
    )

    self.assertIsNone(writer.validate_row(row))


if __name__ == '__main__':
    unittest.main()
