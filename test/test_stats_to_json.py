import os
import unittest
import pytest
from pyspark.sql.types import ArrayType, DoubleType
from analysis_tier.stats_to_json import histogram, process, filepath, minio_client
from pyspark.sql import SparkSession
from pyspark.sql.functions import col


class TestHistogram(unittest.TestCase):

    def test_histogram(self):
        data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        expected_result = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
        col = ArrayType(DoubleType())
        num_bins = 10
        lower_bound = 0
        upper_bound = 10
        actual_result = histogram(col, num_bins, lower_bound, upper_bound)(data)
        self.assertEqual(actual_result, expected_result)


@pytest.fixture(scope='module')
def spark():
    spark = SparkSession.builder \
        .appName("my_test_app") \
        .master("local[2]") \
        .getOrCreate()
    yield spark
    spark.stop()

def test_process(spark):
    # Create a DataFrame with some test data
    data = [(1, 2, 3), (4, 5, 6), (7, 8, 9)]
    df = spark.createDataFrame(data, ['col1', 'col2', 'col3'])

    # Call the function under test
    process(df)

    # Assert that the output is correct
    bucket_name = os.environ['MINIO_BUCKET']
    object_name = f"stats_{filepath}"
    object_exists = minio_client.bucket_exists(bucket_name) and \
                    any(obj.object_name == object_name for obj in minio_client.list_objects(bucket_name))
    assert object_exists == True
