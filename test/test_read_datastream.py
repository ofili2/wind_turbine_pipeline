import unittest

from pyspark.sql import SparkSession
from pyspark.sql.functions import col, to_timestamp

from analysis_tier.read_datastream import process_stream


class TestProcessStream(unittest.TestCase):
    def test_process_stream(self):
        from pyspark.sql.types import StructType, StructField, StringType, TimestampType
        from pyspark.sql.functions import lit
        
        spark = SparkSession.builder.appName("test").getOrCreate()
        
        # Define input and expected output DataFrames
        input_data = [("2022-01-01 00:00:00", "value1"), ("2022-01-01 00:01:00", "value2")]
        schema = StructType([StructField("timestamp", StringType(), True), StructField("value", StringType(), True)])
        input_df = spark.createDataFrame(input_data, schema)
        expected_output_data = [("2022-01-01 00:00:00", "value1"), ("2022-01-01 00:01:00", "value2")]
        expected_output_df = spark.createDataFrame(expected_output_data, schema)
        expected_output_df = expected_output_df.withColumn("timestamp", to_timestamp(col("timestamp"), "yyyy-MM-dd HH:mm:ss").cast(TimestampType()))
        expected_output_df = expected_output_df.withColumn("extra_col", lit(None))
        
        # Apply processing to input DataFrame
        output_df = process_stream(input_df)
        
        # Assert that the output DataFrame matches the expected output DataFrame
        self.assertEqual(output_df.collect(), expected_output_df.collect())


if __name__ == '__main__':
    unittest.main()
