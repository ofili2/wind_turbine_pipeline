import unittest
from unittest.mock import patch, MagicMock

from collection_tier.producer import DataProducer

class TestDataProducer(unittest.TestCase):

    @patch('producer.KafkaProducer')
    def test_send_message(self, kafka_producer_mock):
        # Arrange
        producer = DataProducer()
        message = {'foo': 'bar'}

        # Act
        producer.send_message('key', message)

        # Assert
        kafka_producer_mock.return_value.send.assert_called_once_with(
            'test-topic', key=b'key', value=b'{"foo": "bar"}'
        )

    def test_exit_closes_producer(self):
        # Arrange
        producer = DataProducer()
        producer.producer = MagicMock()

        # Act
        producer.__exit__(None, None, None)

        # Assert
        producer.producer.close.assert_called_once()
