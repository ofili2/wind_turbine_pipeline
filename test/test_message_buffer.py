"""
This script tests the add_message(), is_full(), and timeout() methods of the MessageQueue class.
The test_add_message() function tests adding messages to the buffer, resetting the buffer, and getting messages from the buffer. 
The test_is_full() function tests whether the buffer is full or not. 
The test_timeout() function tests whether the buffer is reset after the timeout period.
"""

import time
from collection_tier.message_buffer import MessageQueue


def test_add_message():
    buffer = MessageQueue(max_len=5, timeout=60)
    buffer.add('message1')
    assert len(buffer) == 1
    assert buffer[0] == 'message1'

    buffer.add('message2')
    buffer.add('message3')
    buffer.add('message4')
    buffer.add('message5')
    assert len(buffer) == 5
    assert buffer[0] == 'message1'
    assert buffer[4] == 'message5'

    buffer.add('message6')
    assert len(buffer) == 1
    assert buffer[0] == 'message6'
    
    buffer.reset()
    assert len(buffer) == 0


def test_is_full():
    buffer = MessageQueue(max_len=3, timeout=60)
    assert not buffer.is_full()
    buffer.add('message1')
    assert not buffer.is_full()
    buffer.add('message2')
    assert not buffer.is_full()
    buffer.add('message3')
    assert buffer.is_full()


def test_timeout():
    buffer = MessageQueue(max_len=5, timeout=1)
    buffer.add('message1')
    assert len(buffer) == 1
    time.sleep(2)
    buffer.add('message2')
    assert len(buffer) == 1
