import unittest
from unittest.mock import MagicMock, patch

from pyspark.sql import Row
from pyspark.sql.functions import avg, window

from analysis_tier.influxdb_writer import InfluxDBWriter
from analysis_tier.pipeline import window_df


class TestMain(unittest.TestCase):
    
    def setUp(self):
        self.mock_row = Row(
            start='2022-03-31T16:00:00Z',
            end='2022-03-31T16:10:00Z',
            generator___mechanical___speed=14.5,
            generator___temperature=62.3,
            turbine_data___electrical___power_production=5000,
            met_sensors___mechanical___wind_speed=8.3,
            met_sensors___temperature___ambient=23.5,
        )
    
    @patch('main.run_stream')
    def test_window_df(self, mock_run_stream):
        # Mock the streaming dataframe
        mock_df = MagicMock()
        mock_df.withWatermark.return_value = mock_df
        mock_df.withColumn.return_value = mock_df
        mock_df.groupBy.return_value = mock_df
        mock_df.agg.return_value = mock_df
        mock_df.select.return_value = mock_df
        mock_df.writeStream.return_value.start.return_value.awaitTermination.return_value = None
        
        mock_run_stream.return_value = mock_df
        
        # Test window_df function
        result = window_df(mock_df)
        
        # Verify that the function calls are correct
        mock_df.withWatermark.assert_called_once_with("timestamp", "5 minutes")
        mock_df.withColumn.assert_called_once_with("window", window("timestamp", "10 minute", "5 minutes"))
        mock_df.groupBy.assert_called_once_with(col("window"))
        mock_df.agg.assert_called_once_with(
            avg(col("generator___mechanical___speed")).alias("avg_generator_speed"),
            avg(col("generator___temperature")).alias("avg_generator_temperature"),
            avg(col("turbine_data___electrical___power_production")).alias("avg_power_production"),
            avg(col("met_sensors___mechanical___wind_speed")).alias("avg_wind_speed"),
            avg(col("met_sensors___temperature___ambient")).alias("avg_ambient_temperature")
        )
        mock_df.select.assert_called_once_with(
            col("window.start").alias("start"),
            col("window.end").alias("end"),
            col("generator___mechanical___speed"),
            col("generator___temperature"),
            col("turbine_data___electrical___power_production"),
            col("met_sensors___mechanical___wind_speed"),
            col("met_sensors___temperature___ambient")
        )
        
        # Verify that the function returns the expected result
        self.assertEqual(result, mock_df)
        
    @patch.object(InfluxDBWriter, "process")
    @patch('main.window_df')
    def test_main(self, mock_window_df, mock_process):
        # Mock the windowed dataframe and InfluxDBWriter
        mock_windowed_df = MagicMock()
        mock_window_df.return_value = mock_windowed_df
        
        mock_influxdb_writer = MagicMock()
        mock_process.return_value = mock_influxdb_writer
        
        # Call the main function
        main()
        
        # Verify that the function calls are correct
        mock_window_df.assert_called_once()
        mock_windowed_df.writeStream.assert_called_once_with.foreach(mock_influxdb_writer)
        mock_process.assert_called_once_with(mock_row)
