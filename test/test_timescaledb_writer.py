import unittest
from pyspark.sql import SparkSession
from analysis_tier.timescaledb_writer import write_to_db


class TestWriteToDB(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.spark = SparkSession.builder.appName("TestWriteToDB").getOrCreate()
        
    @classmethod
    def tearDownClass(cls):
        cls.spark.stop()

    def test_write_to_db(self):
        # Create a sample DataFrame
        data = [(1.0, 2.0, 3.0, 4.0, 5.0, 6.0)]
        df = self.spark.createDataFrame(data, schema=['generator___mechanical___speed', 'generator___temperature', 'turbine_data___electrical___power_production', 'met_sensors___mechanical___wind_speed', 'met_sensors___temperature___ambient'])
        
        # Write DataFrame to test database
        db_config = {
            'host': 'localhost',
            'schema': 'testdb',
            'table': 'testtable',
            'user': 'testuser',
            'password': 'testpass'
        }
        query_name = 'test_query'
        write_stream = write_to_db(df, query_name, db_config)
        
        # Wait for write_stream to start and write some data
        write_stream.awaitTermination(5)
        
        # Assert that the data was successfully written to the database
        result_df = self.spark.read \
            .format("jdbc") \
            .option("url", f"jdbc:postgresql://{db_config['host']}/{db_config['schema']}") \
            .option("dbtable", db_config['table']) \
            .option("user", db_config['user']) \
            .option("password", db_config['password']) \
            .load()
        
        self.assertEqual(result_df.count(), 1)
        
        # Stop write_stream
        write_stream.stop()


if __name__ == '__main__':
    unittest.main()
