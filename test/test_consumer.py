"""
This test uses MagicMock and patch to mock out the KafkaConsumer and CassandraWriter classes, as well as their instances. 
The test sets up the mocks to return None when necessary, and then calls the main function. 
Finally, it asserts that the KafkaConsumer and CassandraWriter classes were initialized with the correct arguments, 
and that CassandraWriter wrote to the database when the buffer was full.
"""


import unittest
from unittest.mock import MagicMock, patch

from collection_tier.consumer import main


class TestMain(unittest.TestCase):

    @patch('my_module.KafkaConsumer')
    @patch('my_module.CassandraWriter')
    def test_main(self, mock_writer, mock_consumer):
        # Set up mock KafkaConsumer
        mock_consumer_instance = MagicMock()
        mock_consumer.return_value = mock_consumer_instance
        mock_consumer_instance.subscribe.return_value = None
        mock_consumer_instance.__next__.return_value = None

        # Set up mock CassandraWriter
        mock_writer_instance = MagicMock()
        mock_writer.return_value = mock_writer_instance
        mock_writer_instance.write.return_value = None

        # Call the main function
        main()

        # Assert that KafkaConsumer was initialized with the correct arguments
        mock_consumer.assert_called_once_with(
            bootstrap_servers=['localhost:9092'],
            group_id='west-sensor',
            auto_offset_reset='earliest'
        )

        # Assert that KafkaConsumer subscribed to the correct topic
        mock_consumer_instance.subscribe.assert_called_once_with(topics=['my_topic'])

        # Assert that CassandraWriter was initialized with the correct arguments
        mock_writer.assert_called_once_with(
            host='localhost',
            port=9042,
            keyspace='my_keyspace',
            table='my_table'
        )

        # Assert that CassandraWriter wrote to the database when the buffer was full
        mock_writer_instance.write.assert_called_once()
