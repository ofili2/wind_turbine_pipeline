import os
from configparser import ConfigParser

from pyspark.context import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import col, to_timestamp

# Get Cassandra connection credentials
config = ConfigParser()
config.read(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.ini'))

host = config.get('cassandra', 'host')
port = config.get('cassandra', 'port')
keyspace = config.get('cassandra', 'keyspace')
table = config.get('cassandra', 'table')

# Configure Spark
conf = SparkConf() \
    .setAppName("Read and Process Wind Turbine Data") \
    .setMaster("spark://spark-master:7077") \
    .set("spark.sql.shuffle.partitions", "100")


def process_stream(stream: DataFrame) -> DataFrame:
    """Apply processing to the input stream.

    Args:
        stream (DataFrame): The input stream to process.

    Returns:
        DataFrame: The processed stream.
    """
    processed_stream = stream \
        .withColumn("timestamp", to_timestamp(col("timestamp"), "yyyy-MM-dd HH:mm:ss").cast("timestamp"))

    return processed_stream


def run_stream() -> DataFrame:
    """Read data from Cassandra, process it and return a streaming DataFrame.

    Returns:
        DataFrame: The processed stream.
    """
    with SparkSession.builder \
            .config(conf=conf) \
            .getOrCreate() as spark:

        # Set Cassandra connection properties
        spark.conf.set("spark.cassandra.connection.host", host)
        spark.conf.set("spark.cassandra.connection.port", port)
        spark.sparkContext.setLogLevel("INFO")

        # Read data from Cassandra
        stream: DataFrame = spark.readStream \
            .format("org.apache.spark.sql.cassandra") \
            .option("table", table) \
            .option("keyspace", keyspace) \
            .load()

        # Process the data
        processed_stream = process_stream(stream)

        return processed_stream
