import logging.config
import os

from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import avg, col, window

from influxdb_writer import InfluxDBWriter
from read_datastream import run_stream

config_path = os.path.dirname(os.path.abspath(__file__))

# Get a logger instance
logging.config.fileConfig(os.path.join(config_path, 'event_logs/logging.ini'))
logger = logging.getLogger(__name__)


# Group with window function
def window_df(df: DataFrame):
    """
    Groups the data with a 10-minute window and a 5-minute slide, and calculates the average values for the generator
    mechanical speed, generator temperature, turbine power production, wind speed, and ambient temperature.
    """
    window_df = df \
        .withWatermark("timestamp", "5 minutes") \
        .withColumn("window", window("timestamp", "10 minute", "5 minutes")) \
        .groupBy(col("window")) \
        .agg(
            avg(col("generator___mechanical___speed")).alias("avg_generator_speed"),
            avg(col("generator___temperature")).alias("avg_generator_temperature"),
            avg(col("turbine_data___electrical___power_production")).alias("avg_power_production"),
            avg(col("met_sensors___mechanical___wind_speed")).alias("avg_wind_speed"),
            avg(col("met_sensors___temperature___ambient")).alias("avg_ambient_temperature")
            ).select(
            col("window.start").alias("start"),
            col("window.end").alias("end"),
            col("generator___mechanical___speed"),
            col("generator___temperature"),
            col("turbine_data___electrical___power_production"),
            col("met_sensors___mechanical___wind_speed"),
            col("met_sensors___temperature___ambient")
        )
    return window_df


def main():
    # Streaming DataFrame
    streaming_df = run_stream()

    # Write data to InfluxDB
    try:
        windowed_df = window_df(streaming_df)
        windowed_df.writeStream.foreach(InfluxDBWriter()).start()
        logger.info("Writing to InfluxDB")
    except Exception as e:
        logger.error(f"Error writing to database: {e}")
        raise e


if __name__ == '__main__':
    main()
