from __future__ import annotations

import datetime
import logging.config
import os

import numpy as np
from minio import Minio
from minio.error import S3Error
from pyspark.sql import DataFrame
from pyspark.sql.functions import mean, stddev, max, udf
from pyspark.sql.types import ArrayType, DoubleType

from read_datastream import run_stream

config_path = os.path.dirname(os.path.abspath(__file__))

# Get a logger instance
logging.config.fileConfig(os.path.join(config_path, 'event_logs/logging.ini'))
logger = logging.getLogger(__name__)

access_key = os.environ['MINIO_ACCESS_KEY']
secret_key = os.environ['MINIO_SECRET_KEY']
host = os.environ['MINIO_HOST']
port = int(os.environ.get('MINIO_PORT', '9000'))
bucket = os.environ['MINIO_BUCKET']

filepath = datetime.datetime.utcnow().strftime('%Y-%m-%d-%H-%M-%S')

batch_size = int(os.environ.get('BATCH_SIZE', '100'))
num_bins = int(os.environ.get('NUM_BINS', '10'))
lower_bound = float(os.environ.get('LOWER_BOUND', '0'))
upper_bound = float(os.environ.get('UPPER_BOUND', '50'))


# Initialize MinIO client
minio_client = Minio(
    endpoint=f"{host}:{port}",
    access_key=access_key,
    secret_key=secret_key,
    secure=False
)


def histogram(col, num_bins, lower_bound, upper_bound):
    """
    Calculate histogram of a column using numpy library.
    
    Args:
        col (pyspark.sql.Column): column to calculate histogram on
        num_bins (int): number of bins to divide the range into
        lower_bound (float): minimum value of the range
        upper_bound (float): maximum value of the range

    Returns:
        pyspark.sql.Column: Column containing the histogram values
    """
    def histogram_func(values):
        hist, edges = np.histogram(values, bins=num_bins, range=(lower_bound, upper_bound))
        return list(hist.astype(np.double))

    return udf(histogram_func, ArrayType(DoubleType()))(col)


def process(streaming_data: DataFrame):
    """
    Process a streaming DataFrame by calculating statistical summaries and uploading to MinIO
    
    Args:
        streaming_data (pyspark.sql.DataFrame): streaming DataFrame to process
    """
    statistics: list[bytes] = []
    for col in streaming_data.columns:
        for i, row in enumerate(streaming_data, start=1):
            if i % batch_size == 0:
                stats = streaming_data.select(
                    mean(col),
                    stddev(col),
                    max(col),
                    histogram(col, num_bins, lower_bound, upper_bound)
                ).toPandas()
                stats_json: str | None = stats.to_json(orient='records')
                statistics.append(stats_json.encode('utf-8'))
                
                # Upload stats to MinIO
                try:
                    minio_client.put_object(
                        bucket_name=bucket,
                        object_name=f"stats_{filepath}",
                        data=statistics,
                        length=len(statistics)
                    )
                except S3Error as e:
                    logger.error(f"Error uploading {filepath}.json: {e}")


if __name__ == '__main__':
    stream_data = run_stream()
    process(stream_data)
