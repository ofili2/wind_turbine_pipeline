import logging
from pymongo import MongoClient


class MongoDBBufferedHandler(logging.Handler):
    def __init__(self, host, port, database, collection, buffer_size=0) -> None:
        super().__init__()
        self.buffer_size = buffer_size
        self.buffer = []
        self.client = MongoClient(host=host, port=port)
        self.database = self.client[database]
        self.collection = self.database[collection]

    def emit(self, record: logging.LogRecord) -> None:
        document = {
            'timestamp': record.created,
            'level': record.levelname,
            'message': self.format(record)
        }
        self.buffer.append(document)
        if len(self.buffer) >= self.buffer_size:
            self.flush()

    def flush(self) -> None:
        if self.buffer:
            self.collection.insert_many(self.buffer)
            self.buffer.clear()


class LevelFilter(logging.Filter):
    def __init__(self, level) -> None:
        self.level = level

    def filter(self, record: logging.LogRecord) -> bool:
        return record.levelno == self.level
