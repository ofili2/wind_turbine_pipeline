import logging.config
import os
import re
import time

from dotenv import load_dotenv, find_dotenv
from influxdb_client import InfluxDBClient
from influxdb_client.client.write.point import DEFAULT_WRITE_PRECISION, Point
from influxdb_client.client.write_api import SYNCHRONOUS
from pyspark.sql import Row

load_dotenv(find_dotenv(raise_error_if_not_found=True))


config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'event_logs/logging.ini')
logging.config.fileConfig(config_path)

# Get a logger instance
logger = logging.getLogger(__name__)


class InfluxDBWriter:
    """
    A class for writing Spark rows to InfluxDB.

    Example usage:

    ```
    with InfluxDBWriter() as influxdb_writer:
        for row in rows:
            influxdb_writer.process(row)
    ```

    """
    def __init__(self):
        """
        Initializes a new instance of the InfluxDBWriter class.

        The InfluxDB connection information and authentication token are read from environment variables.

        Raises:
            ValueError: If the required environment variables are not set.
        """
        influxdb_url = os.getenv("INFLUXDB_URL")
        influxdb_token = os.getenv("INFLUXDB_TOKEN")
        influxdb_org = os.getenv("INFLUXDB_ORG")

        if not influxdb_url:
            raise ValueError("INFLUXDB_URL environment variable not set")
        if not influxdb_token:
            raise ValueError("INFLUXDB_TOKEN environment variable not set")
        if not influxdb_org:
            raise ValueError("INFLUXDB_ORG environment variable not set")

        self.client = InfluxDBClient(
            url=influxdb_url,
            token=influxdb_token,
            org=influxdb_org
        )
        self.write_api = self.client.write_api(write_options=SYNCHRONOUS)

    def __enter__(self):
        """
        Called when the class is used in a `with` statement.
        """
        logger.info("Opening InfluxDB connection")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Called when the class is used in a `with` statement.
        """
        self.write_api.__del__()
        self.client.__del__()
        logger.info("Closing InfluxDB connection")

    def process(self, row: Row):
        """
        Writes a Spark row to InfluxDB.

        Args:
            row: The Spark row to write to InfluxDB.

        Raises:
            ValueError: If the row is missing required fields or if any fields have an invalid format.
        """
        # Validate the row before writing it
        self.validate_row(row)

        # Build the data point to write
        data = {
            "measurement": row["measurement"],
            "time": row["time"],
            "fields": row["fields"],
            "tags": row["tags"],
        }
        point = Point.from_dict(data)

        # Write the data point to InfluxDB
        self.write_api.write(
            bucket="energydata",
            record=[point],
            write_precision=DEFAULT_WRITE_PRECISION
        )
        logger.info("Writing to InfluxDB bucket ...")

    def validate_row(self, row: Row):
        """
        Validates a Spark row before writing it to InfluxDB.

        Args:
            row: The Spark row to validate.

        Raises:
            ValueError: If the row is missing required fields or if any fields have an invalid format.
        """
        # Check if required fields are present
        required_fields = ['time', 'measurement', 'tags', 'fields']
        missing_fields = [f for f in required_fields if f not in row]
        if missing_fields:
            logger.error(f"Missing fields: {','.join(missing_fields)}")
            raise ValueError(f"Missing fields: {','.join(missing_fields)}")
        
        # Check if the time field is in the correct format
        time_format = '%Y-%m-%dT%H:%M:%SZ'
        time_str = row['time']
        try:
            time.strptime(time_str, time_format)
        except ValueError:
            logger.error(f"Invalid time format: {time_str}. Required format: {time_format}")
            raise ValueError(f"Invalid time format: {time_str}. Required format: {time_format}")
        
        # Check if measurement name is valid
        measurement_name = row['measurement']
        if not re.match(r'^[a-zA-Z][a-zA-Z0-9]*$', measurement_name):
            logger.error(f"Invalid measurement name: {measurement_name}. Names must start with a letter and contain only letters, numbers, or underscores.")
            raise ValueError(f"Invalid measurement name: {measurement_name}. Names must start with a letter and contain only letters, numbers, or underscores.")

        # Check if tags and fields are dictionaries
        for key in ['tags', 'fields']:
            value = row[key]
            if not isinstance(value, dict):
                logger.error(f"{key} must be a dictionary")
                raise ValueError(f"{key} must be a dictionary")
