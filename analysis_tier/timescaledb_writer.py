import logging.config
import os
from configparser import ConfigParser
from typing import Any, Dict

from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import month, year
from pyspark.sql.streaming import StreamingQuery
from pyspark.sql.types import StructType, StructField, FloatType, TimestampType

from read_datastream import run_stream

config_path = os.path.dirname(os.path.abspath(__file__))

# Get a logger instance
logging.config.fileConfig(os.path.join(config_path, 'event_logs/logging.ini'))
logger = logging.getLogger(__name__)

# Load database configuration
config = ConfigParser()
config.read(os.path.join(config_path, 'config.ini'))

DB_CONFIG: Dict[str, Any] = {
    'host': config.get('timescaledb', 'host'),
    'schema': config.get('timescaledb', 'schema'),
    'table': config.get('timescaledb', 'table'),
    'user': config.get('timescaledb', 'user'),
    'password': config.get('timescaledb', 'password')
}

# Define database schema
SCHEMA = StructType([
    StructField('timestamp', TimestampType(), False),
    StructField('generator___mechanical___speed', FloatType(), True),
    StructField('generator___temperature', FloatType(), True),
    StructField('turbine_data___electrical___power_production', FloatType(), True),
    StructField('met_sensors___mechanical___wind_speed', FloatType(), True),
    StructField('met_sensors___temperature___ambient', FloatType(), True),
])


def write_to_db(df: DataFrame, query_name: str, db_config: Dict[str, Any]) -> StreamingQuery:
    """
    Writes a Spark DataFrame to a Timescaledb database using a JDBC sink.

    Args:
        df: The DataFrame to write to the database.
        query_name: A unique name for the query.
        db_config: A dictionary containing the database connection details.

    Returns:
        The StreamingQuery object representing the write operation.
    """
    df = df \
        .withColumn("year", year("timestamp")) \
        .withColumn("month", month("timestamp"))

    try:
        # Start the write_stream
        write_stream = df.writeStream \
            .format("jdbc") \
            .option("url", f"jdbc:postgresql://{db_config['host']}/{db_config['schema']}") \
            .option("dbtable", db_config['table']) \
            .option("user", db_config['user']) \
            .option("password", db_config['password']) \
            .queryName(query_name) \
            .trigger(continuous='5 seconds') \
            .start()
        logger.info("Writing to database")
        return write_stream
    except Exception as e:
        logger.error(f"Error writing to database: {e}")
        raise e


def main():
    # Process data stream
    processed_stream = run_stream()

    # Write data to database
    try:
        write_stream = write_to_db(processed_stream, 'streamToTimescaledb', DB_CONFIG)
        write_stream.awaitTermination()
    except KeyboardInterrupt:
        logger.warning("Keyboard interrupt received. Stopping...")
    except Exception as e:
        logger.exception(f"Unexpected error: {e}")
    finally:
        # Stop the write_stream if it is active
        if 'write_stream' in locals() and not write_stream.isActive:
            write_stream.stop()


if __name__ == '__main__':
    main()

