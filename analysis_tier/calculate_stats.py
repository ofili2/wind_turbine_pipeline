import logging.config

from pyspark.ml.feature import StandardScaler, VectorAssembler

from read_datastream import run_stream

logging.config.fileConfig('event_logs/logging.ini')

# Get a logger instance
logger = logging.getLogger(__name__)


# Create a vector assembler to assemble the features column
assembler = VectorAssembler(inputCols=[
    "generator___mechanical___speed",
    "generator___temperature",
    "turbine_data___electrical___power_production",
    "met_sensors___mechanical___wind_speed",
    "met_sensors___temperature___ambient"
], outputCol="features")

# Assemble the features column
data = assembler.transform(run_stream())

# Create a StandardScaler object
scaler = StandardScaler(inputCol="features", outputCol="scaledFeatures", withStd=True, withMean=True)

# Fit the StandardScaler on the data
scalerModel = scaler.fit(data)

# Transform using StandardScaler
scaledData = scalerModel.transform(data)

scaledData.show()
