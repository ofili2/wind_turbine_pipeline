-- Create extension
CREATE EXTENSION IF NOT EXISTS timescaledb;

-- Create schema and table
CREATE SCHEMA energydata;

CREATE TABLE energydata.wind_turbine (
    timestamp TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    year INT,
    month INT,
    generator___mechanical___speed NUMERIC,
    generator___temperature NUMERIC,
    turbine_data___electrical___power_production NUMERIC,
    met_sensors___mechanical___wind_speed NUMERIC,
    met_sensors___temperature___ambient NUMERIC
);

-- Convert table to hypertable with chunk interval
SELECT create_hypertable('energydata.wind_turbine', 'timestamp', chunk_time_interval => INTERVAL '1 day');

-- Create endex on timestamp column
CREATE INDEX ON energydata.wind_turbine (timestamp);

-- Optimize write performance
ALTER TABLE energydata.wind_turbine SET (timescaledb.compress, true);

-- Grant permissions
GRANT ALL PRIVILEGES ON DATABASE oslo_wind_turbine TO postgres;
GRANT ALL PRIVILEGES ON SCHEMA energydata TO postgres;
GRANT ALL PRIVILEGES ON TABLE energydata.wind_turbine TO postgres;

-- Enable SSL connection
ALTER SYSTEM SET ssl = on;

