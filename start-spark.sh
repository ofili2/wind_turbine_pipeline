#!/usr/bin/env bash

. "/opt/spark/bin/load-spark-env.sh"

if [ "$SPARK_WORKLOAD" == "master" ]; then
    SPARK_MASTER_HOST=$(hostname)
    export  SPARK_MASTER_HOST

    cd /opt/spark/bin && ./spark-class org.apache.spark.deploy.master.Master --ip "$SPARK_MASTER_HOST" --port "$SPARK_MASTER_PORT" --webui-port "$SPARK_MASTER_WEBUI_PORT" >> "$SPARK_MASTER_LOG"
elif [ "$SPARK_WORKLOAD" == "worker" ]; then
    cd /opt/spark/bin && ./spark-class org.apache.spark.deploy.worker.Worker --ip "$SPARK_WORKER_WEBUI_PORT" >> "$SPARK_WORKER_LOG"

elif [ "$SPARK_WORKLOAD" == "submit" ]; then
    echo "SPARK SUBMIT"

else
    echo "Undefined Workload Type $SPARK_WORKLOAD, must specify: master, worker, submit"
fi
