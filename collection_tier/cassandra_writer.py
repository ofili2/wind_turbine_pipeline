import logging.config
from contextlib import contextmanager
from datetime import datetime
from os import path
from typing import Dict

from cassandra.auth import PlainTextAuthProvider
from cassandra.cluster import Cluster
from cassandra.cluster import NoHostAvailable
from cassandra.concurrent import execute_concurrent_with_args
from cassandra.policies import RoundRobinPolicy
from cassandra.query import BatchStatement, ConsistencyLevel, BatchType

config_path = path.dirname(path.abspath(__file__))

# Get a logger instance
logging.config.fileConfig(path.join(config_path, 'event_logs/logging.ini'))
logger = logging.getLogger(__name__)


class CassandraConnector:
    """
    Args:
        host (str): The host address of the Cassandra cluster.
        port (int): The port number of the Cassandra cluster.
        username (str): The username for authentication to the Cassandra cluster.
        password (str): The password for authentication to the Cassandra cluster.
        keyspace (str): The keyspace name to connect to.

    Attributes:
        session: The session object obtained after connecting to the Cassandra cluster.
        cluster: The cluster object for the Cassandra cluster.

    Methods:
        connect(): Connects to the Cassandra cluster and gets a session.
        close(): Closes the Cassandra cluster connection and the session.
        get_session(): A context manager to get a session object to execute queries.
        execute_concurrent_with_args(): Executes a query concurrently with arguments
    """

    def __init__(self, host: str, port: int, username: str, password: str, keyspace: str) -> None:
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.keyspace = keyspace
        self.session = None
        self.cluster = None

    def connect(self):
        auth_provider = PlainTextAuthProvider(username=self.username, password=self.password)
        self.cluster = Cluster(
            contact_points=[self.host],
            port=self.port,
            auth_provider=auth_provider,
            load_balancing_policy=RoundRobinPolicy(),
            protocol_version=2,
            connect_timeout=10,
            control_connection_timeout=None
        )
        self.session = self.cluster.connect(self.keyspace)

    def close(self):
        self.cluster.shutdown()
        self.session.shutdown()

    @contextmanager
    def get_session(self):
        if not self.session:
            self.connect()
        try:
            yield self.session
        except Exception as e:
            logger.error(f"Error in session: {str(e)}")
        finally:
            self.close()

    def execute_concurrent_with_args(self, query, args, page_size=5000, num_workers=8):
        """
        Executes a query on the Cassandra cluster using concurrent execution with arguments.

        Parameters:
            query (str): The CQL query to execute.
            args (list): A list of tuples, with each tuple containing the arguments for a single query.
            page_size (int): The size of the pages to use for pagination.
            num_workers (int): The number of worker threads to use for concurrent execution.
        """
        with self.get_session() as session:
            future_results = execute_concurrent_with_args(session, query, args, page_size=page_size,
                                                          num_workers=num_workers)
        for future in future_results:
            try:
                future.result()
            except Exception as e:
                logger.error(f"Cassandra query failed: {str(e)}")


class CassandraWriter:
    def __init__(self, host: str, port: int, keyspace: str, table: str, username: str = None,
                 password: str = None) -> None:
        self.connector = CassandraConnector(host, port, username, password, keyspace)
        self.table = table

    def write(self, message: Dict):
        try:
            with self.connector.get_session() as session:
                query = f"""
                    INSERT INTO {self.table} (
                        timestamp,
                        generator___mechanical___speed, 
                        generator___temperature,
                        turbine_data___electrical___power_production,
                        met_sensors___mechanical___wind_speed,
                        met_sensors___temperature___ambient
                    ) VALUES (?, ?, ?, ?, ?, ?)
                """
                prepared = session.prepare(query)
                batch = BatchStatement(consistency_level=ConsistencyLevel.QUORUM, batch_type=BatchType.LOGGED)
                batch.add(prepared, (
                    datetime.utcfromtimestamp(message["timestamp"]),
                    message["generator___mechanical___speed"],
                    message["generator___temperature"],
                    message["turbine_data___electrical___power_production"],
                    message["met_sensors___mechanical___wind_speed"],
                    message["met_sensors___temperature___ambient"]
                ))
                session.execute(batch)
                logger.info("Record written to Cassandra")
        except NoHostAvailable as e:
            logger.error(f"Cassandra is down: {str(e)}")
        except Exception as e:
            logger.error(f"Cassandra error: {str(e)}")
