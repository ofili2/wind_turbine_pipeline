import threading
import time
from collections import deque


class MessageQueue:
    """
    This class implement a message queue. It uses a data structure that store messages
    in a first-in, first-out (FIFO) order.

    Args:
        max_len (int): The maximum number of elements that can be stored in the queue.
        timeout (float): The amount of time to wait for before flushing the queue.
    """

    def __init__(self, max_len: int, timeout: float) -> None:
        self.max_len = max_len
        self.timeout = timeout
        self.buffer = deque(maxlen=max_len)
        self.last_reset_time = time.monotonic()
        self.lock = threading.Lock()

    def add(self, item):
        """
        Adds the specified item to the queue.
        Args:
            item: Item to be added to the queue.
        """
        with self.lock:
            self.buffer.append(item)
            if len(self.buffer) == self.max_len or time.monotonic() - self.last_reset_time >= self.timeout:
                self.reset()

    def reset(self):
        """
        Clears the queue
        """
        with self.lock:
            self.buffer.clear()
            self.last_reset_time = time.monotonic()

    def get(self):
        """
        Returns the elements from the queue.
        Returns:
            self.buffer: list
        """
        with self.lock:
            return list(self.buffer)
        
    def is_full(self):
        with self.lock:
            return len(self.buffer) == self.max_len
        
    def __len__(self):
        """
        Returns:
            Returns the number of elements in the queue.
        """
        with self.lock:
            return len(self.buffer)
        
    def __iter__(self):
        """
        Returns:
            Returns an iterator for the elements in the queue.
        """
        with self.lock:
            return iter(self.buffer)

    def __getitem__(self, index):
        """
        Returns the elements at the specified index in the queue.
        Args:
            index:

        Returns:
            Elements at the specified index.

        Raises:
            IndexError: If the index is out of range
        """
        with self.lock:
            return self.buffer[index]
