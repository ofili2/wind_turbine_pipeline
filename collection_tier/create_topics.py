import logging.config
from os import path

from kafka.admin import KafkaAdminClient, NewTopic
from kafka.errors import TopicAlreadyExistsError

from config import BOOTSTRAP_SERVERS, TOPIC, NUM_PARTITIONS, REPLICATION_FACTOR

config_path = path.dirname(path.abspath(__file__))

# Get a logger instance
logging.config.fileConfig(path.join(config_path, 'event_logs/logging.ini'))
logger = logging.getLogger(__name__)


def create_topics():
    """
    Creates Kafka topics using the provided configuration.

    Raises:
    Exception: If there is an error while creating the topics.
    """
    try:
        # Create KafkaAdminClient
        client = KafkaAdminClient(
            bootstrap_servers=BOOTSTRAP_SERVERS,
            # security_protocol="SSL",
            # ssl_cafile="ca.pem",
            # ssl_certfile="service.cert",
            # ssl_keyfile="service.key",
            # sasl_mechanism="PLAIN",
            # sasl_plain_username="admin",
            # sasl_plain_password="secret"
        )

        # Define the topics to be created
        new_topics = (
            NewTopic(name=topic, num_partitions=NUM_PARTITIONS, replication_factor=REPLICATION_FACTOR)
            for topic in TOPIC
        )

        # Create the topics
        client.create_topics(new_topics)

        logger.info(f"{len(TOPIC)} topics created successfully: {', '.join(TOPIC)}")

    except Exception as e:
        if isinstance(e, TopicAlreadyExistsError):
            logger.warning(f"Topic(s) {e.topics} already exist(s). Skipping creation.")
        else:
            logger.error(f"An error occurred while creating topics: {str(e)}")
            raise e


if __name__ == '__main__':
    create_topics()
