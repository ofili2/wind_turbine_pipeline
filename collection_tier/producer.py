import json
import logging.config
import os
import uuid
from os import path

import pandas as pd
from kafka import KafkaProducer
from kafka.errors import KafkaError, NoBrokersAvailable

from config import BOOTSTRAP_SERVERS, TOPIC
from redis_logger import MessageLogger

config_path = path.dirname(path.abspath(__file__))
logging.config.fileConfig(path.join(config_path, 'event_logs/logging.ini'))

# Get a logger instance
logger = logging.getLogger(__name__)


class DataProducer:
    """
    A Kafka producer for sending data messages to a Kafka topic.

    Attributes:
    -----------
    producer : KafkaProducer
        An instance of KafkaProducer that is used to send messages to Kafka.
    topic : str
        The name of the Kafka topic to which messages will be sent.

    Methods:
    --------
    send_message(key, message)
        Sends a message to the Kafka topic with the specified key and message.
    """
    def __init__(self):
        """
        Initializes the DataProducer by creating an instance of KafkaProducer
        and setting the Kafka topic to which messages will be sent.
        """
        try:
            self.producer = KafkaProducer(
                bootstrap_servers=BOOTSTRAP_SERVERS,
                key_serializer=json_encoder,
                value_serializer=lambda v: json.dumps(v).encode('utf-8'),
                compression_type='gzip',
            )
        except NoBrokersAvailable as e:
            raise Exception(f"Unable to connect to Kafka brokers: {e}")
        self.topic = TOPIC[0]
        self.topic_handler = TopicHandler(self.topic)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Closes the KafkaProducer instance when the with block is exited.
        """
        self.producer.close()

    def send_message(self, key, message):
        """
        Sends a message to the Kafka topic with the specified key and message.

        Parameters:
        -----------
        key : bytes
            The key that will be used to send the message.
        message : dict
            The message to be sent to the Kafka topic.

        Raises:
        -------
        KafkaError : If an error occurs while sending the message.
        """
        # key_bytes = self.topic_handler.get_key_bytes(key) if key else None
        future = self.producer.send(self.topic, key=key, value=message)
        try:
            result = future.get(timeout=10)
            logger.info('Successfully sent message=%s to topic=%s', message, self.topic)
            return result
        except KafkaError as e:
            logger.error(str(e))


class TopicHandler:
    """
    Handler for Kafka producer send completion events.

    Attributes:
    -----------
    key : bytes
        The key of the message sent.

    Methods:
    --------
    on_completion(metadata, exception=None)
    Handles Kafka producer send completion events.
    """
    def __init__(self, key):
        """
        Initializes the TopicHandler with the specified key.
        """
        assert type(key) in (bytes, bytearray, memoryview, type(None)), \
            f"Expected bytes, bytearray, memoryview, or NoneType, but got {type(key)}"
        self.key = key

    def on_completion(self, metadata, exception=None):
        """
        Handles Kafka producer send completion events by logging errors or moving
        messages to the failed message log.

        Parameters:
        -----------
        metadata : RecordMetadata or None
            The metadata for the message if it was successfully sent, None otherwise.
        exception : Exception or None
            The exception that occurred while sending the message, None otherwise.
        """
        if exception is not None:
            # mark record as failed
            MessageLogger.moved_to_failed(self.key)
        elif metadata is not None:
            # remove the data from the local state
            try:
                MessageLogger.remove_event(self.key)
            except Exception as e:
                logger.error(str(e))
        else:
            # mark record as failed
            MessageLogger.moved_to_failed(self.key)

    def to_dict(self):
        return {'key': self.key}


def json_encoder(obj):
    if isinstance(obj, TopicHandler):
        return obj.to_dict()
    else:
        raise TypeError(f"{obj} is not JSON serializable")


if __name__ == '__main__':
    # Initialize Kafka producer
    with DataProducer() as producer:
        # Read message from CSV
        data_chunks = pd.read_csv(config_path + '/oslo_wind_turbine.csv', chunksize=1000)
        chunk = next(data_chunks)
        for idx, row in chunk.iterrows():
            message = row.to_dict()

        # Convert DataFrame to dictionary and send message to topic
            key = uuid.uuid4().bytes
            print(type(key))
            producer.send_message(key, message)
            logger.info("Message sent, key=%s, value=%s", key, message)
