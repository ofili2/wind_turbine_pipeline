import logging.config
import os

import rocksdb

config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'event_logs/logging.ini')
logging.config.fileConfig(config_path)

# Get a logger instance
logger = logging.getLogger(__name__)


class MessageLogger:
    transient_state_db = None
    failed_state_db = None
    transient_state_db_path = 'transient.db'
    failed_state_db_path = 'failed.db'

    @staticmethod
    def initialize():
        try:
            options = rocksdb.Options()
            options.create_if_missing = True
            options.max_open_files = 300000
            options.write_buffer_size = 67108864
            options.max_write_buffer_number = 3
            options.target_file_size_base = 67108864

            options.table_factory = rocksdb.BlockBasedTableFactory(
                filter_policy=rocksdb.BloomFilterPolicy(10),
                block_cache=rocksdb.LRUCache(2 * (1024 ** 3)),
                block_cache_compressed=rocksdb.LRUCache(500 * (1024 ** 2))
            )

            with rocksdb.DB(MessageLogger.transient_state_db_path, options) as transient_state_db, \
                    rocksdb.DB(MessageLogger.failed_state_db_path, options) as failed_state_db:
                MessageLogger.transient_state_db = transient_state_db
                MessageLogger.failed_state_db = failed_state_db
        except Exception as e:
            logger.error("Error while initializing RocksDB: {}".format(e))
            raise e

    @staticmethod
    def ensure_directories():
        if not os.path.exists(MessageLogger.transient_state_db_path):
            os.makedirs(MessageLogger.transient_state_db_path)
        if not os.path.exists(MessageLogger.failed_state_db_path):
            os.makedirs(MessageLogger.failed_state_db_path)

    @staticmethod
    def log_transient_state(key, value):
        with MessageLogger.transient_state_db.write_batch() as batch:
            batch.put(bytes(key, 'utf-8'), value)

    @staticmethod
    def log_failed_state(key, value):
        with MessageLogger.failed_state_db.write_batch() as batch:
            batch.put(bytes(key, 'utf-8'), value)

    @staticmethod
    def get_transient_state(key):
        return MessageLogger.transient_state_db.get(bytes(key, 'utf-8'))

    @staticmethod
    def get_failed_state(key):
        return MessageLogger.failed_state_db.get(bytes(key, 'utf-8'))

    @staticmethod
    def add_event(key, data):
        try:
            with MessageLogger.transient_state_db.write_batch() as batch:
                key_bytes = bytes(key, 'utf-8')
                value = MessageLogger.transient_state_db.get(key_bytes)
                if value is not None:
                    batch.put(key_bytes, data)
        except rocksdb.errors.NotFound as e:
            logger.error(str(e))
            raise e

    @staticmethod
    def remove_event(key):
        try:
            with MessageLogger.transient_state_db.write_batch() as batch:
                key_bytes = bytes(key, 'utf-8')
                value = MessageLogger.transient_state_db.get(key_bytes)
                if value is not None:
                    batch.delete(key_bytes)
        except rocksdb.errors.NotFound as e:
            logger.error(str(e))
            raise e

    @staticmethod
    def moved_to_failed(key):
        try:
            key_bytes = bytes(key, 'utf-8')
            value = MessageLogger.transient_state_db.get(key)
            if value is not None:
                MessageLogger.failed_state_db.put(key_bytes, value)
                # remove it from transient_state_db
                try:
                    MessageLogger.transient_state_db.delete(key_bytes)
                except Exception as e:
                    logger.error(str(e))
        except Exception as e:
            logger.error(str(e))
            raise e

    @staticmethod
    def close():
        if MessageLogger.transient_state_db is not None:
            MessageLogger.transient_state_db.close()
            MessageLogger.transient_state_db = None
        if MessageLogger.failed_state_db is not None:
            MessageLogger.failed_state_db.close()
            MessageLogger.failed_state_db = None
        if MessageLogger.options is not None:
            MessageLogger.options.close()
            MessageLogger.options = None
