import logging
from pymongo import MongoClient


class MongoDBBufferedHandler(logging.Handler):
    """
    A buffered logging handler that writes log records to a MongoDB collection.

    Attributes:
        buffer_size (int): The maximum number of log records to buffer before flushing to the MongoDB collection.
        buffer (list): The buffer of log records.
        client (pymongo.MongoClient): The MongoDB client instance.
        database (pymongo.database.Database): The MongoDB database instance.
        collection (pymongo.collection.Collection): The MongoDB collection instance.
    """

    def __init__(self, host, port, database, collection, buffer_size=0) -> None:
        """
        Initialize a new MongoDBBufferedHandler instance.

        Args:
            host (str): The MongoDB server hostname.
            port (int): The MongoDB server port number.
            database (str): The name of the MongoDB database to write log records to.
            collection (str): The name of the MongoDB collection to write log records to.
            buffer_size (int): The maximum number of log records to buffer before flushing to the MongoDB collection.
        """
        super().__init__()
        self.buffer_size = buffer_size
        self.buffer = []
        self.client = MongoClient(host=host, port=port)
        self.database = self.client[database]
        self.collection = self.database[collection]

    def emit(self, record: logging.LogRecord) -> None:
        """
        Write the log record to the buffer.

        Args:
            record (logging.LogRecord): The log record to write.
        """
        document = {
            'timestamp': record.created,
            'level': record.levelname,
            'message': self.format(record)
        }
        self.buffer.append(document)
        if len(self.buffer) >= self.buffer_size:
            self.flush()

    def flush(self) -> None:
        """
        Flush the buffer of log records to the MongoDB collection.
        """
        if self.buffer:
            self.collection.insert_many(self.buffer)
            self.buffer.clear()


class LevelFilter(logging.Filter):
    """
    A logging filter that filters log records by level.

    Attributes:
        level (int): The log level to filter by.
    """

    def __init__(self, level) -> None:
        """
        Initialize a new LevelFilter instance.

        Args:
            level (int): The log level to filter by.
        """
        self.level = level

    def filter(self, record: logging.LogRecord) -> bool:
        """
        Filter log records by level.

        Args:
            record (logging.LogRecord): The log record to filter.

        Returns:
            bool: True if the log record level is equal to the filter level, False otherwise.
        """
        return record.levelno == self.level
