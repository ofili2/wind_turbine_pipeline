import logging.config
import os

import redis

config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'event_logs/logging.ini')
logging.config.fileConfig(config_path)

# Get a logger instance
logger = logging.getLogger(__name__)


class MessageLogger:
    redis_conn = None
    redis_host = 'localhost'
    redis_port = 6379
    redis_db = 0

    @staticmethod
    def initialize():
        try:
            MessageLogger.redis_conn = redis.Redis(host=MessageLogger.redis_host, port=MessageLogger.redis_port, db=MessageLogger.redis_db)
            MessageLogger.redis_conn.ping()
        except redis.exceptions.ConnectionError as e:
            logger.error("Error while initializing Redis: {}".format(e))
            raise e

    @staticmethod
    def log_transient_state(key, value):
        try:
            MessageLogger.redis_conn.set(key, value)
        except redis.exceptions.RedisError as e:
            logger.error("Error while logging transient state to Redis: {}".format(e))
            raise e

    @staticmethod
    def log_failed_state(key, value):
        try:
            MessageLogger.redis_conn.set(key, value)
        except redis.exceptions.RedisError as e:
            logger.error("Error while logging failed state to Redis: {}".format(e))
            raise e

    @staticmethod
    def get_transient_state(key):
        try:
            return MessageLogger.redis_conn.get(key)
        except redis.exceptions.RedisError as e:
            logger.error("Error while getting transient state from Redis: {}".format(e))
            raise e

    @staticmethod
    def get_failed_state(key):
        try:
            return MessageLogger.redis_conn.get(key)
        except redis.exceptions.RedisError as e:
            logger.error("Error while getting failed state from Redis: {}".format(e))
            raise e

    @staticmethod
    def add_event(key, data):
        try:
            if MessageLogger.redis_conn.get(key) is not None:
                MessageLogger.redis_conn.set(key, data)
        except redis.exceptions.RedisError as e:
            logger.error("Error while adding event to Redis: {}".format(e))
            raise e

    @staticmethod
    def remove_event(key):
        try:
            if MessageLogger.redis_conn.get(key) is not None:
                MessageLogger.redis_conn.delete(key)
        except redis.exceptions.RedisError as e:
            logger.error("Error while removing event from Redis: {}".format(e))
            raise e

    @staticmethod
    def moved_to_failed(key):
        try:
            value = MessageLogger.redis_conn.get(key)
            if value is not None:
                MessageLogger.redis_conn.set(key, value, ex=None, nx=True, xx=False)
        except redis.exceptions.RedisError as e:
            logger.error("Error while moving event to failed state in Redis: {}".format(e))
            raise e

    @staticmethod
    def close():
        if MessageLogger.redis_conn is not None:
            MessageLogger.redis_conn.close()
            MessageLogger.redis_conn = None
