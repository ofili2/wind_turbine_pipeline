import logging.config
import time
from os import path

from kafka import KafkaConsumer
from kafka.errors import NoBrokersAvailable

from cassandra_writer import CassandraWriter
from config import (
    BOOTSTRAP_SERVERS, CASSANDRA_HOST, CASSANDRA_PORT,
    CASSANDRA_KEYSPACE, CASSANDRA_TABLE, MAX_LEN, TIMEOUT, TOPIC)
from message_buffer import MessageQueue

config_path = path.dirname(path.abspath(__file__))

# Get a logger instance
logging.config.fileConfig(path.join(config_path, 'event_logs/logging.ini'))
logger = logging.getLogger(__name__)


def main():
    """
    Main function that initializes a Kafka consumer, a message buffer, and a Cassandra writer.
    The function continuously consumes messages from the Kafka topic, adds them to the buffer,
    and writes the buffer to the Cassandra cluster when the buffer reaches its maximum length or
    timeout is reached.
    """
    try:
        # Initialize Kafka consumer
        consumer = KafkaConsumer(
            bootstrap_servers=BOOTSTRAP_SERVERS,
            group_id='west-sensor',
            auto_offset_reset='earliest'
        )

        # Subscribe to Kafka topic
        consumer.subscribe(topics=TOPIC)

        # Create buffer with max length and timeout
        buffer = MessageQueue(max_len=MAX_LEN, timeout=TIMEOUT)

        # Initialize Cassandra writer
        writer = CassandraWriter(
            host=CASSANDRA_HOST, port=CASSANDRA_PORT, 
            keyspace=CASSANDRA_KEYSPACE, table=CASSANDRA_TABLE
        )

        while True:
            logger.info("Consuming messages from %s...", TOPIC[0])
            for messages in consumer:
                # Add the message value to the buffer
                buffer.add(messages.value)
                if messages.value is None:
                    logger.error("Waiting for messages to consume")
                else:
                    logger.info("Message sent, value=%s", messages.value)

                # Write the buffer to the Cassandra cluster
                if len(buffer.buffer) == buffer.max_len or time.monotonic() - buffer.last_reset_time >= buffer.timeout:
                    writer.write(buffer.get())
    except NoBrokersAvailable as e:
        logger.error(str(e))
        raise Exception("Unable to connect to Kafka brokers. Check the configuration.")


if __name__ == '__main__':
    main()
