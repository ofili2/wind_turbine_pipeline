#!/bin/bash

# Retrieve MongoDB root username and password from MongoDB instance
MONGO_ROOT_USERNAME=$(mongo --quiet --eval "printjson(db.getSiblingDB('admin').auth('$MONGO_INITDB_ROOT_USERNAME', '$MONGO_INITDB_ROOT_PASSWORD'));
printjson(db.getUser('$MONGO_INITDB_ROOT_USERNAME', { showCredentials: true }))" | grep "user" -A 1 | tail -n 1 | awk -F"" '{print $4}')
MONGO_ROOT_PASSWORD=$(mongo --quiet --eval "printjson(db.getUser('$MONGO_INITDB_ROOT_USERNAME', { showCredentials: true }))" | grep "password" | awk -F "" '{print $4}')

# Set MongoDB username and password for Compass
export MONGO_ROOT_USERNAME=$MONGO_INITDB_ROOT_USERNAME
export MONGO_ROOT_PASSWORD=$MONGO_INITDB_ROOT_PASSWORD

# Start MongoDB Compass application
/usr/bin/mongod --sslMode requireSSL --sslPEMKeyFile /ssl/mongodb.pem
